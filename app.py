from flask import Flask, jsonify, request
app = Flask(__name__)

@app.route('/')
def home():
    return 'My MLE02 Home page - Welcome!!!'

if __name__ == '__main__':
    app.run()